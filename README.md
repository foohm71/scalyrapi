# scalyrAPI

## What is this?
A simple postgres API packaged as a docker container to access a postgres DB

How to run the docker container:
```
docker run -e SCALYR_TOKEN=<scalyr Read log token> -it -p 5000:33 foohm71/scalyrapi:main
```

You will need to get the Read log token from your Scalyr admin.

## How to call the API

The `test.sh` shows you how to make the call to the API. It takes in a payload `test.json`
test.sh looks like this:
```
curl -X POST "http://localhost:5000/query" -d @test.json --header "Content-Type: application/json"
```

`test.json` looks like this:
```
{"filter": <filter>}
```

This would translate to a Scalyr filter you use to search your logs.

## What's with the other files
This docker image was built using a Python Notebook - ScalyrAPI.ipynb. It uses the python `requests` library to access the Scalyr API and Flask to build the API. It is then converted to python using nbconvert and packaged into a docker image as described in `Dockerfile`. To see how the image is built see `.gitlab-ci.yml`.